FROM alpine:3.4

ENV MANET_PROXY_VERSION 0.7.1
ENV UWSGI_PROFILE core

RUN apk add --no-cache --virtual .build-deps ca-certificates gcc musl-dev openssl-dev linux-headers py-pip \
    && apk add --no-cache --virtual .run-deps uwsgi-python \
    && pip install https://bitbucket.org/shoreware/manet-proxy/get/${MANET_PROXY_VERSION}.tar.gz \
    && apk del .build-deps

ADD uwsgi.ini /etc/manet/manet-proxy.ini

EXPOSE 5000

CMD ["/usr/sbin/uwsgi", "--ini", "/etc/manet/manet-proxy.ini", "--plugins-dir", "/usr/lib/uwsgi", "--plugins", "python"]
