# About this Repo

This is the Git repo of the official Docker image for
[manet-proxy](https://registry.hub.docker.com/u/shore/manet-proxy/). See the Hub
page for the full readme on how to use the Docker image and for information
regarding contributing and issues.
